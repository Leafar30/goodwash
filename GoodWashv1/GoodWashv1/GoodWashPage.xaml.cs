﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GoodWashv1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GoodWashPage : ContentPage
	{
		public GoodWashPage ()
		{
			InitializeComponent ();
            LoadGoodWash();
		}

        public async void LoadGoodWash()
        {
            var httpClient = new HttpClient();
            var response =  await httpClient.GetStringAsync("http://dataservice.accuweather.com/currentconditions/v1/252066?apikey=wWpWRAGeaSE6LSYWrAlCpSAm70a2DCL9");
            var goodwashes = JsonConvert.DeserializeObject<List<GoodWash>>(response);
            LvGoodWash.ItemsSource = goodwashes;
        }
    }
}